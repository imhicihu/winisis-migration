## MicroIsis / WinIsis:

* [El manual de CDS/ISIS para Windows](https://eprints.mdx.ac.uk/3077/7/Spanish_Handbook.pdf) Andrew Buxton & Alan Hopkinson. Accedido el 14/12/2017 
* Manual Winisis: versión 1997. Córdoba: Universidad Nacional de Córdoba. Secretaría de Administración. Departamento Cómputos, 1997. 63 h.; 30 cm.  (vide #22428) ===> Solicitar a/por: `Depósito Proatlas 016321`
* Manual de referencia mini-micro CDS/ISIS: versión 2.3. Paris: Unesco, 1990. xvi, 352 p.; 25 cm. --) ===> Solicitar a/por: `Depósito Proatlas 006204`
* Bibliografía de publicaciones editadas en 1993: incorporadas a la base de datos de PROATLAS. Buenos Aires: PROATLAS, mar 1994. 2, 34, 8, 3, 15 h.; 28 cm.  ===> Solicitar a/por: `Depósito Proatlas 006800`
* Bibliografía del material ingresado en la base de datos "TERRAE": 1992-1993. Buenos Aires: PROATLAS, set 1994. 2 v.; 28 cm. ===> Solicitar a/por: `Depósito Proatlas 006801`
* Jornadas Nacionales sobre Microisis. 3., Mar del Plata, 1993. ===> Solicitar a/por: `Depósito Proatlas 006802`
* [Manual para el ingreso de Información en WinIsis a la base de datos bibliográfica](http://www.bvsde.paho.org/bvsair/e/manuales/winisis/paqwinis.pdf). Organización Mundial de la Salud. Organización Panamericana de la Salud. Area de Desarrollo Sostenible y Salud Ambiental 
* Uso de la función REF en campos de autor institucional: en la base de datos TERRAE. En: Jornadas Nacionales sobre MicroIsis. 3., Mar del Plata, 1993. Actas. 1993. p. 101-108. ===> Solicitar a/por: `Depósito Proatlas 006803`


## Database:
* _Open Refine_. Ruben Verborgh, Max De Wilde, 116 pp. ISBN: 139781783289080. PackPress.

## Miscellaneous
* [Acceso a bases Isis desde Python, vía WXIS](http://catalis.uns.edu.ar/doku/doku.php/acceso_a_bases_isis_desde_python)